<?php

include "../includes/getclasses.inc.php";

if (session_status() == PHP_SESSION_ACTIVE) { 
    header("Location: dashboard.php");
    exit();
} else {
    header("Location: login.php");
}

?>


<!-- 
infinite parameters in php
function infinite_parameters() {
    foreach (func_get_args() as $param) {
        echo "Param is $param" . PHP_EOL;
    }
}

to specify
function infinite_parameters() {
    echo func_get_arg(2);
} 

check if parameter exists
function infinite_parameters() {
    if (func_num_args() < 3) {
        throw new BadFunctionCallException("Not enough parameters!");
    }
}


-->
