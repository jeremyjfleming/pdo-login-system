<?php
include_once "../includes/getclasses.inc.php";

// to-do for this page:
// global- add return value for controller

if (!$_GET) {

    Utilities::checkSession(true);

    echo "<form action='resetpassword.php' method='post' name='resetPasswordEmail'>
    <input type='email' name='email' placeholder='Email' required>
    <input type='submit' name='submit' value='Submit'>
</form>";

    if (isset($_POST['submit'])) {

        $generateTokenEmail = new ResetPassword($_POST['email']);
        $generateTokenEmail->generateTokenEmail();

        // if returns true -> display message. if not true, display error message.
    }


}
//
if ($_GET) {
    if (isset($_GET['new'])) {
        if (!empty($_GET['token']) && !empty($_GET['vcode']) && !empty($_GET['email'])) {
            Utilities::checkSession(true);

            $token = $_GET['token'];
            $vcode = $_GET['vcode'];
            $tokenView = new View();
            $data = $tokenView->showData("SELECT * FROM tokens WHERE token = ?", $token);

            if (empty($data) || $vcode != $data['vcode'] || $date("U") - $data['timestamp'] > 900 || $data['isUsed'] == 0)
                echo "Invalid Token";
            else {

                // if there is an active session then redirect to the dashboard

                echo "<form method='post' action='resetpassword.php' name='newPasswordNotLoggedIn' onsubmit='return checkPasswords()'>
    <input type='password' name='password1' placeholder='Enter a new Password'>
    <input type='password' name='password2' placeholder='Re-enter the password'>
    <input type='submit' name='submit' value='Submit'></form>

    <script >
    let password1 = document.forms['newPasswordNotLoggedIn']['password1'];
    let password2 = document.forms['newPasswordNotLoggedIn']['password2'];

    function checkPasswords() {
        if (password1 !== password2) {
            return false;
        }
    }
</script>

    ";

                if (isset($_POST['submit'])) {

                    $newPassword = new ResetPassword($_GET['email']);
                    $newPassword->newPassword($_POST['password1']);

                    // if returns true -> display message, redirect to login. if not true, display error message and update token table

                    $updateUsed = new Controller();
                    $updateUsed->insertData("UPDATE tokens SET isUsed = ? WHERE email = ?", 1, $_GET['email']);
                }

            }
        } elseif (!empty($_GET))
            header("Location: resetpassword.php");
    }
//
    if (isset($_GET['change'])) {
        if (!empty($_GET['token2']) && !empty($_GET['vcode2']) && session_status() == PHP_SESSION_ACTIVE) {
            // if there is no active session, assume this get request was a mistake and redirect to mail resetpassword page.

            $token2 = $_GET['token2'];
            $vcode2 = $_GET['vcode2'];
            $tokenView2 = new View();
            $data2 = $tokenView2->showData("SELECT * FROM tokens WHERE token = ?", $token2);

            if (empty($data2) || $vcode != $data2['vcode'] || $date("U") - $data['timestamp'] > 900 || $data2['isUsed'] == 0)
                echo "Invalid Token";
            else {

                echo "<form method='post' action='resetpassword.php' name='newPasswordLogged' onsubmit='return checkPasswords()'>
    <input type='password' name='password1' placeholder='Enter a new Password'>
    <input type='password' name='password2' placeholder='Re-enter the password'>
    <input type='submit' name='submit' value='Submit'></form>

    <script >
    let password1 = document.forms['newPasswordLogged']['password1'];
    let password2 = document.forms['newPasswordLogged']['password2'];

    function checkPasswords() {
        if (password1 !== password2) {
            return false;
        }
    }
</script>

    ";

                if (isset($_POST['submit'])) {

                    $newPassword = new ResetPassword($_SESSION['email']);
                    $newPassword->newPassword($_POST['password1']);

                    // if returns true -> display message, redirect back to manage.php. if not true, display error message.
                }
            }
        } elseif (session_status() == PHP_SESSION_ACTIVE) { // if this elseif was reached, one of the checks didnt pass. check
            //if there is still an active session because if so the user should not be on this page. or if there is no session,
            // redirect so long as there is a get request.
            header("Location: manage.php");
        } elseif (session_status() != PHP_SESSION_ACTIVE) {
            if (!empty($_GET))
                header("Location: resetpassword.php");
        }
    }
}

