<?php 


class Controller extends Model {

    public function insertData($query, ...$params) {

        $this->setData($query, $params);

    }

    public function createAccount($userId, $firstName, $lastName, $userName, $email, $password) {
        $sql = 'INSERT INTO accounts(userId, firstName, lastName, username, email, password) VALUES(?, ?, ?, ?, ?, ?)';
        $data = [$userId, $firstName, $lastName, $userName, $email, $password];
        $this->setData($sql, $data);
    }

}