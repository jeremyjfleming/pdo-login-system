<?php

class Dbh {
    private $host = "localhost";
    private $username = "root";
    private $password = "";
    private $database = "main";

    protected function conn() {
        $dsn = "mysql:host={$this->host};dbname={$this->database};";
        $pdo = new PDO($dsn, $this->username, $this->password) or die('Could not connect to database' . pdo_connect_error());
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    }
}

