<?php

class Login extends View {

    private $inputemail;
    private $inputpassword;

    public function __construct($email, $password) {

        $this->inputemail = $email;
        $this->inputpassword = $password;

    }
    public function login() {

        if (empty($this->inputemail))
            return "Email cannot be empty";
        elseif (!filter_var($this->inputemail, FILTER_VALIDATE_EMAIL))
            return "Invalid email";
        elseif (empty($this->inputpassword))
            return "Password cannot be empty";

        $this->inputpassword = password_hash($this->inputpassword, PASSWORD_DEFAULT);

        $data = $this->showData("SELECT * FROM accounts WHERE email = ?", $this->inputemail);
        if (!$data)
            return "Could not find account in database. Create an account or reset password to continue";

        $userId = $data['userId'];
        $firstName = $data['firstName'];
        $lastName = $data['lastName'];
        $username = $data['username'];
        $databaseemail = $data['email'];
        $databasepassword = $data['password'];


        if ($this->inputpassword != $databasepassword)
            return "Incorrect password. Passwords can be reset using the link below.";

        session_start();

        Utilities::setSessions($userId, $firstName, $lastName, $username, $databaseemail);

        header("Location: dashboard.php");

        return true;
    }

}

//


