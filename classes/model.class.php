<?php

class Model extends Dbh {

    protected function getData($query, $params) {
        $stmt = $this->conn()->prepare($query);
        $stmt->execute($params);
        return $stmt->fetch();
    }

    protected function setData($query, $params) {
        $stmt = $this->conn()->prepare($query);
        $stmt->execute($params);

    }
}

// first name, last name, , username, email, password 