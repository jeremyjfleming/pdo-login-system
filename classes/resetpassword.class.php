<?php


class ResetPassword {

    private $email;

    public function __construct($email) {

        $this->email = $email;

    }

    public function generateTokenEmail() {

        $accountsView = new View();
        $data = $accountsView->showData("SELECT * FROM accounts WHERE email = ?", $this->email);

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL))
            return "Invalid Email";
        elseif ($data['email'] == '')
            return "Could not find account. ";

        $token = Utilities::generateString(24);
        $vcode = Utilities::generateInt();
        $timestamp = date("U");

        $to = $this->email;
        $subject = "Subject";
        $message = "/newpassword.php?new&email={$this->email}&token={$token}&vcode={$vcode}";
        $headers = "From: Signum <service@signum.com>\r\nReply-To: service@signum.com\r\n;Content-type: text/html\r\n";
        mail($to, $subject, $message, $headers);

        $tokenController = new Controller();
        $tokenController-> insertData("INSERT INTO tokens(email, token, vcode, timestamp) VALUES(?, ?, ?, ?)", $email, $token, $vcode, $timestamp);

        return true;
    }

    public function newPassword($password) {

        $accountsView = new View();
        $accountsData = $accountsView->showData("SELECT * FROM accounts WHERE email = ?", $this->email);

        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
        if ($accountsData['password'] == $password)
            return "New password cannot be the same as the old one.";
        $insertDataNew = new Controller;
        $insertDataNew->insertData("UPDATE accounts SET password = ? WHERE email = ?", $hashedPassword, $this->email);

        return true;
    }

}