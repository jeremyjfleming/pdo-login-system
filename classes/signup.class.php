<?php

class Signup extends Controller {

    private $userId;
    private $firstName;
    private $lastName;
    private $username;
    private $email;
    private $password;

    public function __construct($firstName, $lastName, $username, $email, $password) {
        $this->userId = Utilities::generateString();
        $this->firstName = ucfirst($firstName);
        $this->lastName = ucfirst($lastName);
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;

    }
    public function signup() {

        $signupdata = new View();
        $signupdata->showData("SELECT * FROM accounts WHERE email = ?", $email);

        if (empty($this->firstName))
            return "First Name cannot be empty";
        elseif (strlen($this->firstName) >= 100)
            return "First Name cannot be more than 100 characters";
        elseif (empty($this->lastName))
            return "Last Name cannot be empty";
        elseif (strlen($this->$lastName) >= 100)
            return "Last Name cannot be more than 100 characters";
        elseif (empty($this->$username))
            return "Username cannot be empty";
        elseif (strlen($this->$username) >= 100)
            return "Username cannot be more than 100 characters";
        elseif (empty($email))
            return "Email cannot be empty";
        elseif (strlen($this->$email) >= 100)
            return "First Name cannot be more than 100 characters";
        elseif (!filter_var($this->$email, FILTER_VALIDATE_EMAIL))
            return "Invalid email";
        elseif (!$signupdata = "")
            return "An account already exists with this email address";
        elseif (empty($this->$password))
            return "Password cannot be empty";
        elseif (strlen($this->$password) >= 100)
            return "First Name cannot be more than 100 characters";

        $this->password = password_hash($this->password, PASSWORD_DEFAULT);

        $this->createAccount($this->userId, $this->firstName, $this->lastName, $this->username, $this->email, $this->password);
        session_start();
        Utilities::setSessions($this->userId, $this->firstName, $this->lastName, $this->username, $this->email);

        return true;

    }

}
