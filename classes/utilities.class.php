<?php

class Utilities {

    public static function setSessions($userId, $firstName, $lastName, $userName, $email) {

        $_SESSION['userId'] = $userId;
        $_SESSION['firstName'] = $firstName;
        $_SESSION['lastName'] = $lastName;
        $_SESSION['username'] = $userName;
        $_SESSION['email'] = $email;

    }

    public static function checkSession($isActive = false)
    {
        if ($isActive = false)
            if (!session_status() == PHP_SESSION_ACTIVE) {
                header("Location: login.php");
                exit();

            }
        if ($isActive = true)
            if (session_status() == PHP_SESSION_ACTIVE) {
                header("Location: dashboard.php");
            }

    }
    public static function generateString($length = 18):string {
        $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public static function generateInt($length = 10) {
        $numbers = "123456789";
        $randomInt = '';
        for ($i = 0; $i < $length; $i++) {
            $randomInt .=  $numbers[rand(0, 8)];
        }
        return $randomInt;
    }
}

